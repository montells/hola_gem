# frozen_string_literal: true

module HolaMontells
  class Error < StandardError; end

  # Say hello given a specified language
  #
  # @param language [Symbol] the desired language
  # @return [String] the greeting on the expected language
  def self.hi(language = :spanish)
    translator = Translator.new(language)
    translator.hi
  end
end

require 'hola_montells/translator'
require 'hola_montells/version'
