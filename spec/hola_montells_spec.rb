RSpec.describe HolaMontells do
  it "has a version number" do
    expect(HolaMontells::VERSION).not_to be nil
  end

  describe 'hi' do
    it 'say hello world when language parameter is english' do
      expect(HolaMontells.hi('english')).to eq 'hello world'
    end
    it 'say hello world when language parameter is any value different to nil' do
      expect(HolaMontells.hi('english')).to eq 'hello world'
    end
    it 'say hola mundo when no language parameter' do
      expect(HolaMontells.hi).to eq 'hola mundo'
    end
  end
end
