# HolaMontells

Thanks for watching here

This is just first gem built. I hope more will come.

Not useful value. I was just following the first [how to build a gem](https://guides.rubygems.org/make-your-own-gem/) tutorial

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'hola_montells'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install hola_montells

## Usage

    require 'hola_montells'
    HolaMontells.hi
    HolaMontells.hi(:englis)

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
